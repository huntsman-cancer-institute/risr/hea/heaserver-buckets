# HEA Server Buckets Microservice
[Research Informatics Shared Resource](https://risr.hci.utah.edu), [Huntsman Cancer Institute](https://hci.utah.edu),
Salt Lake City, UT

The HEA Server Buckets Microservice is a service for managing buckets and their data within the cloud.

## Version 1.6.3
* Call the heaserver-folders-aws-s3 service to delete bucket objects.

## Version 1.6.2
* Fixed 500 error when attempting to populate collaborators and when there is no authorization info.

## Version 1.6.1
* Performance improvements.

## Version 1.6.0
* Added support for python 3.12.

## Version 1.5.0
* Removed most integration tests because they overlapped with the unit tests.
* Accept the data query parameter for get requests for a speed boost.

## Version 1.4.6
* Dependency upgrades for compatibility with heaserver-keychain 1.5.0.

## Version 1.4.5
* Missed a place where json needed to be replaced by orjson.

## Version 1.4.4
* Permissions calculation speedup.

## Version 1.4.3
* Caching optimizations.

## Version 1.4.2
* Don't create collaborator groups, roles, etc. if the user has non-collaborator bucket access already.
* Changed the group paths to /Collaborator/AWS Accounts/account id/user id.

## Version 1.4.1
* Addressed potential concurrency issues.
* Fixed bug causing duplicate collaborators in an organization.

## Version 1.4.0
* We now support adding and removing collaborators who are not an admin/manager/member/PI of the organization so that
they can access selected buckets.

## Version 1.3.4
* Improved performance getting a bucket.

## Version 1.3.3
* Fixed type hint errors.
* Updated AWS region list.
* Restored accurate bucket permissions feature.

## Version 1.3.2
* Avoid timeouts loading buckets, which sporadically caused buckets not to be returned.

## Version 1.3.1
* Install setuptools first during installation.
* Correct issue where some users lost access to buckets because the user lacked permissions in AWS to simulate permissions. Instead, such users will appear to receive full permission for everything, which was the behavior prior to version 1.3.0. As before, AWS will still reject requests that users lack permission for.

## Version 1.3.0
* Present accurate bucket permissions.

## Version 1.2.0
* Added deletecontents query parameter the bucket delete endpoint. If true/yes/y, requesting a bucket delete will
delete the bucket's contents first. If false/no/n or omitted, the bucket delete will succeed only if the bucket is
empty.

## Version 1.1.4
* Grant all users DELETER privileges to buckets so that the webclient permits deletion (the backend will still deny
users without permissions to delete buckets).

## Version 1.1.3
* Fixed potential issue preventing the service from updating temporary credentials.

## Version 1.1.2
* Fixed new folder form submission.

## Version 1.1.1
* Display type display name in properties card, and return the type display name from GET calls.

## Version 1.1.0
* Fixed support for uploading to a bucket.
* Pass desktop object permissions back to clients.

## Version 1.0.4
* Changed presented bucket owner to system|aws.
* Omitted shares from the properties template.

## Version 1.0.3
* Improved performance getting all buckets.

## Version 1.0.2
* Improved performance.

## Version 1.0.1
* Improved performance.

## Version 1
Initial release.

## Runtime requirements
* Python 3.10, 3.11, or 3.12.

## Development environment

### Build requirements
* Any development environment is fine.
* On Windows, you also will need:
    * Build Tools for Visual Studio 2019, found at https://visualstudio.microsoft.com/downloads/. Select the C++ tools.
    * git, found at https://git-scm.com/download/win.
* On Mac, Xcode or the command line developer tools is required, found in the Apple Store app.
* Python 3.10, 3.11, or 3.12: Download and install Python from https://www.python.org, and select the options to install
for all users and add Python to your environment variables. The install for all users option will help keep you from
accidentally installing packages into your Python installation's site-packages directory instead of to your virtualenv
environment, described below.
* Create a virtualenv environment using the `python -m venv <venv_directory>` command, substituting `<venv_directory>`
with the directory name of your virtual environment. Run `source <venv_directory>/bin/activate` (or `<venv_directory>/Scripts/activate` on Windows) to activate the virtual
environment. You will need to activate the virtualenv every time before starting work, or your IDE may be able to do
this for you automatically. **Note that PyCharm will do this for you, but you have to create a new Terminal panel
after you newly configure a project with your virtualenv.**
* From the project's root directory, and using the activated virtualenv, run `pip install wheel` followed by
  `pip install -r requirements_dev.txt`. **Do NOT run `python setup.py develop`. It will break your environment.**

### Running tests
Run tests with the `pytest` command from the project root directory. To improve performance, run tests in multiple
processes with `pytest -n auto`.

### Running integration tests
* Install Docker
* On Windows, install pywin32 version >= 223 from https://github.com/mhammond/pywin32/releases. In your venv, make sure that
`include-system-site-packages` is set to `true`.

### Trying out the APIs
This microservice has Swagger3/OpenAPI support so that you can quickly test the APIs in a web browser. Do the following:
* Install Docker, if it is not installed already.
* Run the `run-swaggerui.py` file in your terminal. This file contains some test objects that are loaded into a MongoDB
  Docker container.
* Go to `http://127.0.0.1:8080/docs` in your web browser.

Once `run-swaggerui.py` is running, you can also access the APIs via `curl` or other tool. For example, in Windows
PowerShell, execute:
```
Invoke-RestMethod -Uri http://localhost:8080/buckets/ -Method GET -Headers @{'accept' = 'application/json'}`
```
In MacOS or Linux, the equivalent command is:
```
curl -X GET http://localhost:8080/buckets/ -H 'accept: application/json'
```

### Packaging and releasing this project
See the [RELEASING.md](RELEASING.md) file for details.
